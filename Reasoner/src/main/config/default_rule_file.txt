type : new rule
name: Transitive
id: 1
premise: ?p <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#TransitiveProperty>
premise: ?a ?p ?b
premise: ?b ?p ?c
conclusion: ?a ?p ?c

type : new rule
name: Symmetric
id: 2
premise: ?p <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#SymmetricProperty>
premise: ?a ?p ?b
conclusion: ?b ?p ?a


type : new rule
name: inverseOf
id: 3
premise: ?p1 <http://www.w3.org/2002/07/owl#inverseOf> ?p2
premise: ?a ?p2 ?b
conclusion: ?b ?p1 ?a

type : new rule
name: subClass
id: 4
premise: ?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class>
premise: ?a <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?b
premise: ?b <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class>
premise: ?b <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?c
premise: ?c <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class>
conclusion: ?a <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?c

type : new rule
name: InverseFunctionalProperty
id: 5
premise: ?p1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#InverseFunctionalProperty>
premise: ?y ?p1 ?x
premise: ?z ?p1 ?x
conclusion: ?y <http://www.w3.org/2002/07/owl#sameAs> ?z

type : new consistency rule
name: inconsistency
id: 6
premise: ?a <http://www.w3.org/2002/07/owl#sameAs> ?b
premise: ?a <http://www.w3.org/2002/07/owl#differentFrom> ?b
conclusion: false

type : new rule
name: FunctionalProperty
id: 7
premise: ?p <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#FunctionalProperty>
premise: ?x ?p ?y
premise: ?x ?p ?z
filter: ?y != ?z
conclusion: ?y <http://www.w3.org/2002/07/owl#sameAs> ?z


type : new rule
name: EquivalentProperty
id: 9
premise: ?p1 <http://www.w3.org/2002/07/owl#equivalentProperty> ?p2
conclusion: ?p1 <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?p2
conclusion: ?p2 <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?p1

type : new rule
name: ComplexSubClassEquality
id: 10
premise: ?c1 <http://www.w3.org/2002/07/owl#hasValue> ?i
premise: ?c1 <http://www.w3.org/2002/07/owl#onProperty> ?p1
premise: ?c2 <http://www.w3.org/2002/07/owl#hasValue> ?i
premise: ?c2 <http://www.w3.org/2002/07/owl#onProperty> ?p2
premise: ?p1 <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?p2
filter: ?c1 != ?c2
conclusion: ?c1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?c2

type : new consistency rule
name: ClassInconsistency
id: 11
premise: ?x <http://www.w3.org/2002/07/owl#maxCardinality> "0"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger>
premise: ?x <http://www.w3.org/2002/07/owl#onProperty> ?p
premise: ?u <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?x
premise: ?u ?p ?y
conclusion: false

type : new rule
name: ComplexEquivalentclass
id: 12
premise: ?p1 <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?p2
premise: ?class1 <http://www.w3.org/2002/07/owl#equivalentClass> ?equiClass1
premise: ?equiClass1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Restriction>
premise: ?equiClass1 <http://www.w3.org/2002/07/owl#onProperty> ?p1
premise: ?equiClass1 <http://www.w3.org/2002/07/owl#minCardinality> ?card1
premise: ?class2 <http://www.w3.org/2002/07/owl#equivalentClass> ?equiClass2
premise: ?equiClass2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Restriction>
premise: ?equiClass2 <http://www.w3.org/2002/07/owl#onProperty> ?p2
premise: ?equiClass2 <http://www.w3.org/2002/07/owl#minCardinality> ?card2
filter: ?card1 >= ?card2
conclusion: ?class1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?class2

type: new rule
name: subProp
id: 13
premise: ?x ?p1 ?y
premise: ?p1 <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?p2
conclusion: ?x ?p2 ?y

type : new rule
name: ComplexEquivalentclassNew
id: 14
premise: ?class1 <http://www.w3.org/2002/07/owl#equivalentClass> ?equiClass1
premise: ?equiClass1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Restriction>
premise: ?equiClass1 <http://www.w3.org/2002/07/owl#onProperty> ?p1
premise: ?equiClass1 <http://www.w3.org/2002/07/owl#minCardinality> ?card1
premise: ?class2 <http://www.w3.org/2002/07/owl#equivalentClass> ?equiClass2
premise: ?equiClass2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Restriction>
premise: ?equiClass2 <http://www.w3.org/2002/07/owl#onProperty> ?p1
premise: ?equiClass2 <http://www.w3.org/2002/07/owl#minCardinality> ?card2
filter: ?card1 >= ?card2
conclusion: ?class1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?class2

type: new rule
name: unionTwoClasses
id: 15
premise: ?superClass <http://www.w3.org/2002/07/owl#unionOf> ?union 
premise: ?union <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?subClass1 
premise: ?union <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ?rest1 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?subClass2 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> <http://www.w3.org/1999/02/22-rdf-syntax-ns#nil> 
conclusion: ?subClass1  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass 
conclusion: ?subClass2  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass 

type: new rule
name: unionThreeClasses
id: 16
premise: ?superClass <http://www.w3.org/2002/07/owl#unionOf> ?union 
premise: ?union <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?subClass1
premise: ?union <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ?rest1 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?subClass2 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ?rest2 
premise: ?rest2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?subClass3 
premise: ?rest2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> <http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>
conclusion: ?subClass1  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass 
conclusion: ?subClass2  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass 
conclusion: ?subClass3  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass 

type: new rule
name: intersectionTwoClasses
id: 17
premise: ?subClass <http://www.w3.org/2002/07/owl#intersectionOf> ?intersection 
premise: ?intersection <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?superClass1 
premise: ?intersection <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ?rest1 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?superClass2 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> <http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>
conclusion: ?subClass  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass1 
conclusion: ?subClass  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass2

type: new rule
name: intersectionThreeClasses
id: 18
premise: ?subClass <http://www.w3.org/2002/07/owl#intersectionOf> ?intersection 
premise: ?intersection <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?superClass1 
premise: ?intersection <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ?rest1 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?superClass2 
premise: ?rest1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ?rest2 
premise: ?rest2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?superClass3 
premise: ?rest2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> <http://www.w3.org/1999/02/22-rdf-syntax-ns#nil> 
conclusion: ?subClass  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass1 
conclusion: ?subClass  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass2 
conclusion: ?subClass  <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?superClass3 

type: new rule
name: someValuesFrom
id: 19
premise: ?class <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Restriction> 
premise: ?class <http://www.w3.org/2002/07/owl#onProperty> ?property 
premise: ?class <http://www.w3.org/2002/07/owl#someValuesFrom> ?otherClass 
premise: ?instance ?property ?value 
premise: ?value <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?otherClass 
conclusion: ?instance <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?class 	
	
type: new rule
name: allValuesFrom
id: 20
premise: ?class <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Restriction> 
premise: ?class <http://www.w3.org/2002/07/owl#onProperty> ?property 
premise: ?class <http://www.w3.org/2002/07/owl#allValuesFrom> ?otherClass 
premise: ?instance <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?class 
premise: ?instance ?property ?value 	
conclusion: ?value <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?otherClass

type: new rule
name: equivalentClassForFirstClass
id: 21
premise: ?class1 <http://www.w3.org/2002/07/owl#equivalentClass> ?class2
premise: ?instance <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?class1
conclusion: ?instance <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?class2

type: new rule
name: equivalentClassForSecondClass
id: 22
premise: ?class1 <http://www.w3.org/2002/07/owl#equivalentClass> ?class2
premise: ?instance <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?class2
conclusion: ?instance <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?class1

type: new rule
name: equivalentSymmetric
id: 23
premise: ?class1 <http://www.w3.org/2002/07/owl#equivalentClass> ?class2
conclusion: ?class2 <http://www.w3.org/2002/07/owl#equivalentClass> ?class1
