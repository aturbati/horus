package it.uniroma2.reasoner.utils;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.*;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.query.Binding;
import it.uniroma2.art.owlart.query.BooleanQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.reasoner.domain.InferenceRule;
import it.uniroma2.reasoner.domain.Triple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Utility Class to support operation on Reasoning.
 * 
 * @author Giovanni Lorenzo Napoleoni
 * 
 */
public class OntologyUtilis {

	private static final String SELECT_OPERATION = "SELECT";
	private static final String WHERE_CLAUSOLE = "WHERE";
	private static final String FILTER_CLAUSOLE = "FILTER";

	/**
	 * Convert an InferenceRule in a Sparql query
	 * 
	 * @param inferenceRule
	 *            inference rule to convert
	 * @return inference rule converted
	 */
	public static String createSPARQLQueryFromInferenceRule(InferenceRule inferenceRule) {

		StringBuilder query = new StringBuilder();
		query.append(SELECT_OPERATION);
		query.append(" ");
		// get the variable from inference rule premises
		for (String var : getVarFromInferenceRule(inferenceRule)) {
			query.append(var + " ");

		}
		query.append(WHERE_CLAUSOLE);
		query.append("{ \n");
		// convert each inference rule premises into sparql where clause
		for (Triple triple : inferenceRule.getPremisisTriple()) {
			query.append(triple.getSubject() + " ");
			query.append(triple.getPredicate() + " ");
			query.append(triple.getObject() + ".\n");
		}
		// Check filter
		if (!inferenceRule.getFilterCondition().equals("")) {
			query.append(FILTER_CLAUSOLE + "( ");
			query.append(inferenceRule.getFilterCondition() + ")");
		}
		query.append("}");
		// return sparql query
		return query.toString();
	}

	/**
	 * Get from inference rule all variables
	 * 
	 * @param inferenceRule
	 * @return list of all variables
	 */
	private static HashSet<String> getVarFromInferenceRule(InferenceRule inferenceRule) {

		// List to store variable
		HashSet<String> varsList = new HashSet<String>();

		// Search variable in each premise
		for (Triple triple : inferenceRule.getPremisisTriple()) {

			// For subject,predicate,object, check if they are a var. If it is true, check if the variable
			// is a new or old variable, if it is new, saves it.
			if (isVar(triple.getSubject())) {

				if (!varsList.contains(triple.getSubject())) {

					varsList.add(triple.getSubject());
				}
			}
			if (isVar(triple.getPredicate())) {

				if (!varsList.contains(triple.getPredicate())) {
					varsList.add(triple.getPredicate());
				}
			}
			if (isVar(triple.getObject())) {

				if (!varsList.contains(triple.getObject())) {
					varsList.add(triple.getObject());
				}
			}
		}

		return varsList;
	}

	/**
	 * Convert a tuple generated from sparql query on BaseRDFTriplemodel ontology in ARTStatments. This
	 * conversion is based on a list of triples and bindings operation from variable on triples and variables
	 * into tuple
	 * 
	 * @param tuple
	 *            query result from sparql interrogation
	 * @param triples
	 *            list of where to take the binding variables
	 * @param baseRDFTripleModel
	 *            ontology model
	 * @return a set of ARTStatement that represents the binding conversion of variables
	 */
	public static List<ARTStatement> tupleToArtStatament(TupleBindings tuple, List<Triple> triples,
			BaseRDFTripleModel baseRDFTripleModel) {

		// List to store final results
		List<ARTStatement> statments = new ArrayList<ARTStatement>();
		// Map: associates to each variable its value
		Map<String, ARTNode> varMap = new HashMap<String, ARTNode>();
		// Populate map with variables into tuple
		for (String string : tuple.getBindingNames()) {
			varMap.put(string, tuple.getBinding(string).getBoundValue());
		}
		// For each triple: search all the values ​​to be assigned to the variables in the triple in the
		// values
		// ​​of variables within the tuple.
		for (Triple triple : triples) {

			ARTResource subject;
			ARTURIResource predicate;
			ARTNode object;

			// If subject or object or predicate contains a variable, get the value from map
			subject = fromStringToArtNode(triple.getSubject(), varMap, baseRDFTripleModel).asResource();
			predicate = fromStringToArtNode(triple.getPredicate(), varMap, baseRDFTripleModel).asURIResource();
			object = fromStringToArtNode(triple.getObject(), varMap, baseRDFTripleModel);
			
			
			/*if (varMap.get(triple.getSubject().replaceFirst("\\?", "")) != null) {
				subject = varMap.get(triple.getSubject().replaceFirst("\\?", "")).asResource();

			} else {
				subject = baseRDFTripleModel.createURIResource(triple.getSubject()).asResource();
			}
			if (varMap.get(triple.getPredicate().replaceFirst("\\?", "")) != null) {
				predicate = varMap.get(triple.getPredicate().replaceFirst("\\?", "")).asURIResource();
			} else {
				predicate = baseRDFTripleModel.createURIResource(triple.getPredicate());
			}
			if (varMap.get(triple.getObject().replaceFirst("\\?", "")) != null) {

				object = varMap.get(triple.getObject().replaceFirst("\\?", ""));
			} else {
				object = baseRDFTripleModel.createURIResource(triple.getObject().toString()).asResource();

			}*/

			statments.add(baseRDFTripleModel.createStatement(subject, predicate, object));
		}

		return statments;

	}
	
	public static ARTNode fromStringToArtNode(String value, Map<String, ARTNode> varMap, 
			BaseRDFTripleModel baseRDFTripleModel){
		ARTNode artNode;
		if (varMap.get(value.replaceFirst("\\?", "")) != null) {
			artNode = varMap.get(value.replaceFirst("\\?", ""));
		} else {
			//TODO check the right type for the conversion
			artNode = baseRDFTripleModel.createURIResource(Sanitazer.sanitizeURI(value));
		}
		
		return artNode;
	} 

	public static String convertARTStatementToSimpleString(ARTStatement statement) {
		String subject = getSimpleValueFromTripleItem(statement.getSubject());
		String predicate = getSimpleValueFromTripleItem(statement.getPredicate());
		String object = getSimpleValueFromTripleItem(statement.getObject());

		//System.out.println("dentro convertARTStatementToSimpleString: "+subject + " " + predicate + " " + object); // da cancellare
		
		return subject + " " + predicate + " " + object;
	}
	
	public static String convertARTNodeToString(ARTNode artNode){
		String simpleString = "";
		if(artNode.isURIResource()){
			//simpleString = Sanitazer.sanitizeURILocalName(artNode.asURIResource());
			simpleString = artNode.asURIResource().getLocalName();
		} else if(artNode.isLiteral()){
			ARTLiteral artLiteral = artNode.asLiteral();
			simpleString = "\""+artLiteral.getNominalValue()+"\"";
			if(artLiteral.getDatatype() != null){
				simpleString += "^^"+artLiteral.getDatatype().getURI();
			} else if(artLiteral.getLanguage() != null){
				simpleString += "@"+artLiteral.getLanguage();
			}
		} else if(artNode.isBlank()){
			simpleString = artNode.getNominalValue();
		}
			
		return simpleString;
	}

	public static String getSimpleValueFromTripleItem(ARTNode item) {
		//System.out.println("dentro getSimpleValueFromTripleItem e item = "+item.getNominalValue()); // da cancellare
		
		if (item.isBlank()) {
			//return "BlankNode:" + item.asBNode().toString();
			return item.asBNode().toString();
		}
		if (item.isLiteral()) {
			return item.asLiteral().getNominalValue();
		}
		if (item.isResource()) {
			//System.out.println("dentro getSimpleValueFromTripleItem e item.asURIResource().getNominalValue() = "+item.asURIResource().getNominalValue()); // da cancellare
			//System.out.println("dentro getSimpleValueFromTripleItem e item.asURIResource().getLocalName() = "+item.asURIResource().getLocalName()); // da cancellare
			
			//return Sanitazer.sanitizeURILocalName(item.asURIResource());
			return item.asURIResource().getLocalName();
		}
		//return Sanitazer.sanitizeURILocalName(item.asURIResource());
		return item.asURIResource().getLocalName();

	}

	public static String convertARTStatementToString(ARTStatement statement) {

		/*String subject = Sanitazer.sanitizeURI(statement.getSubject().asURIResource());
		String predicate = Sanitazer.sanitizeURI(statement.getPredicate().asURIResource());
		String object = Sanitazer.sanitizeURI(statement.getObject().asURIResource());*/

		String subject = convertARTNodeToString(statement.getSubject());
		String predicate = convertARTNodeToString(statement.getPredicate());
		String object = convertARTNodeToString(statement.getObject());
		
		return subject + " " + predicate + " " + object;
	}

	/*public static String getValueFromTripleItem(ARTNode item) {
		if (item.isBlank()) {
			return "BlankNode:" + item.asBNode().toString();
		}
		if (item.isLiteral()) {
			return item.asLiteral().toString();
		}
		if (item.isResource()) {
			return item.asURIResource().getURI();
		}
		return item.asURIResource().getURI();

	}*/

	public static boolean isVar(String subject) {
		if (subject.startsWith("?")) {
			return true;
		} else {
			return false;
		}
	}

	public static String getFilterCondition(TupleBindings tuple, String filterCondition) {
		if (!filterCondition.equals("")) {

			Map<String, ARTNode> varMap = new HashMap<String, ARTNode>();
			// Populate map with variables into tuple
			for (String string : tuple.getBindingNames()) {

				if (filterCondition.contains("?" + string)) {
					String key = "?" + string;
					// filterCondition =
					// filterCondition.replace("?"+string,tuple.getBinding(string).getBoundValue().toString());
					filterCondition = filterCondition.replaceAll("\\b" + key + "\\b", tuple
							.getBinding(string).getBoundValue().toString());

				}

			}
			filterCondition = filterCondition.replaceAll("\\?", "");
			return filterCondition;
		}
		return "";
	}

	public static String createASKSPARQLQuery(ARTStatement statement) {

		//String subject = Sanitazer.replaceURI(RDFNodeSerializer.toNT(statement.getSubject()));
		//String predicate = Sanitazer.replaceURI(RDFNodeSerializer.toNT(statement.getPredicate()));;
		//String object = Sanitazer.replaceURI(RDFNodeSerializer.toNT(statement.getObject()));

		String subject = RDFNodeSerializer.toNT(statement.getSubject());
		String predicate = RDFNodeSerializer.toNT(statement.getPredicate());
		String object = RDFNodeSerializer.toNT(statement.getObject());

		
    	String query = "ASK " +
    			"\nWHERE " +
    			"\n{" +
                "\n"+subject+" "+predicate+" "+object+" . "+
                "\n}";

		return query;
	}
	
	public static boolean executePseudoASKSPARQLQueryAndCheckExistence(ARTStatement statement, 
			BaseRDFTripleModel model) throws UnsupportedQueryLanguageException, ModelAccessException, 
			MalformedQueryException, QueryEvaluationException{
		ARTNode subject = statement.getSubject();
		ARTNode predicate = statement.getPredicate();
		ARTNode object = statement.getObject();
		
		//check if at least one of the node of the statement is a BNode
		if(!hasABnode(statement)){
			String query = createASKSPARQLQuery(statement);
			BooleanQuery booleanQuery  = model.createBooleanQuery(QueryLanguage.SPARQL, query, model.getBaseURI());
			return booleanQuery.evaluate(false);
		}
		//there is at least one BNode
		boolean isSubjBNode = subject.isBlank();
		boolean isPredBNode = predicate.isBlank();
		boolean isObjBNode = object.isBlank();
		
		String query = "SELECT * "+
				"\nWHERE{" +
				"\n";
		
		if(isSubjBNode){
			query+="?subj";
		} else{
			query+=RDFNodeSerializer.toNT(subject);
		}
		if(isPredBNode){
			query+=" ?pred";
		}else{
			query+=" "+RDFNodeSerializer.toNT(predicate);
		}
		if(isObjBNode) {
			query+=" ?obj .";
		} else{
			query+=" "+RDFNodeSerializer.toNT(object)+" .";
		}
		query+="\n}";
		TupleQuery tupleQuery = model.createTupleQuery(QueryLanguage.SPARQL, query, model.getBaseURI());
		TupleBindingsIterator tupleIter = tupleQuery.evaluate(false);
		while(tupleIter.hasNext()){
			boolean result = true;
			TupleBindings tuple = tupleIter.getNext();
			if(isSubjBNode){
				ARTNode subjFromQuery = tuple.getBinding("subj").getBoundValue();
				result = result && subjFromQuery.getNominalValue().equals(subject.getNominalValue());
			}
			if(isPredBNode){
				ARTNode predFromQuery = tuple.getBinding("pred").getBoundValue();
				result = result && predFromQuery.getNominalValue().equals(predicate.getNominalValue());
			}
			if(isObjBNode){
				ARTNode objFromQuery = tuple.getBinding("obj").getBoundValue();
				result = result && objFromQuery.getNominalValue().equals(object.getNominalValue());
			}
			if (result){
				tupleIter.close();
				return true;
			}
		}
		tupleIter.close();
		
		return false;
	}
	
	
	public static boolean hasABnode(ARTStatement statement){
		ARTNode subject = statement.getSubject();
		ARTNode predicate = statement.getPredicate();
		ARTNode object = statement.getObject();
		
		return subject.isBlank() || predicate.isBlank() || object.isBlank();
	}
}
